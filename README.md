# loopia-ddns-updater

Ansible role that creates DynDNS Python scripts for Loopia, and configures
a systemd timer to continuously check the DynDNS settings in the background.

I have only used this for IPv4, I don't think this script even supports IPv6.

Loopia does not appear to provide a DynDNS script that supports IPv6, but
a developer has shared theirs on the web:

+ https://joelpurra.com/projects/loopia-api-dyndns
+ https://github.com/joelpurra/loopia-api-dyndns


You can use `icanhazip.com` to find out your IPv6 address as well as IPv4:

+ To check your external IPv4 address: `https://ipv4.icanhazip.com`.
+ To check your external IPv6 address: `https://ipv6.icanhazip.com`.


## Refs

+ https://support.loopia.se/wiki/loopiadns-med-dynamisk-ip/
+ https://major.io/2011/01/15/icanhazip-com-now-supports-both-ipv4-and-ipv6-addresses/
+ https://tailscale.com/kb/1134/ipv6-faq/
